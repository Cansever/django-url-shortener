USAGE

Available Methods	Example URLs

Project Endpoints		
1.	POST /create-shortener-url/	http://localhost:8000/create-shortener-url/
2.	GET	/<shortener_url>	http://localhost:8000/<shortener_url>
3.  GET /shortener-url-list/ http://localhost:8000/shortener-url-list/

1. You can generate short url for your url.
```
    headers: key=Content-Type, value=application/json
    method:POST
    Sample Body
    {
  	   "long_url":"https://www.yemeksepeti.com/city/istanbul"
    }
```

2. You will be redirected to long url with short url in browser.
```
   Sample Request -> http://localhost:8000/OdsIUtcl
```
3. You will get all saved url list data.
```
    headers: key=Content-Type, value=application/json
    Method:GET

    Sample response
    {
        "count": 4,
        "next": "http://localhost:8000/shortener-url-list/?page=2",
        "previous": null,
        "results": [
            {
                "long_url": "https://www.yemeksepeti.com/city/bursa",
                "short_url": "tFxBAK4P",
                "visitor_count": 2
            },
            {
                "long_url": "https://www.yemeksepeti.com/city/ankara",
                "short_url": "FqnpbmTC",
                "visitor_count": 0
            },
            {
                "long_url": "https://www.yemeksepeti.com/city/amasya",
                "short_url": "XQZCael2",
                "visitor_count": 1
            }
        ]
    }
```

