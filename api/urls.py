from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^create-shortener-url/$', views.ShortenerUrlView.as_view()),
    url(r'^shortener-url-list/$', views.ShortenerUrlListView.as_view()),
    url(r'^(?P<url>[\w\-]+)/$', views.RedirectUrlView.as_view()),
]
