from django.db import models

# Create your models here.
class UrlShortener(models.Model):
    long_url = models.CharField(max_length=255)
    short_url = models.CharField(max_length=10)
    visitor_count = models.IntegerField(default=0)

    def __str__(self):
        return self.short_url
