from django.shortcuts import render, redirect
from django.db import transaction
from api.models import UrlShortener
from rest_framework import generics
from api.serializers import UrlShortenerCreateSerializer, UrlShortenerListSerializer
from api.utils import *
from rest_framework.views import APIView
from rest_framework.response import Response
from api.paginators import CustomPagination
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect, Http404


class ShortenerUrlView(APIView):
    serializer_class = UrlShortenerCreateSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        short_url = generate_shorten_url()
        if data['long_url'].endswith('/'):
                data['long_url'] = data['long_url'][:-1]
        url_data = UrlShortener.objects.create(
                long_url=data['long_url'],
                short_url=short_url
        ),
        short_url = "http://{}/".format(get_current_site(self.request).domain) + short_url
        return Response({'short_url': short_url})


class ShortenerUrlListView(generics.ListAPIView):
    serializer_class = UrlShortenerListSerializer
    pagination_class = CustomPagination

    @transaction.atomic
    def get_queryset(self):
        queryset = UrlShortener.objects.all().order_by("-id")
        return queryset


class RedirectUrlView(APIView):

    @transaction.atomic
    def get(self, request, *args, **kwargs):
        url = kwargs.get('url')

        try:
            short_url = UrlShortener.objects.get(short_url=url)
        except UrlShortener.DoesNotExist:
            raise Http404

        short_url.visitor_count = short_url.visitor_count + 1
        short_url.save(update_fields=['visitor_count'])
        return HttpResponseRedirect(short_url.long_url)
