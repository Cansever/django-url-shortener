from .models import UrlShortener
import random


def generate_shorten_url():
        short_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"
        short_url = ("".join(random.sample(short_chars, 8)))
        while True: # url checking is unique
            if UrlShortener.objects.filter(short_url=short_url).exists():
                short_url = ("".join(random.sample(short_chars, 8)))
            else:
                break
        return short_url
