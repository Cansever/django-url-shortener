from rest_framework.serializers import ModelSerializer
from .models import UrlShortener

class UrlShortenerCreateSerializer(ModelSerializer):

    class Meta:
        model = UrlShortener
        fields = ('long_url',)

class UrlShortenerListSerializer(ModelSerializer):

    class Meta:
        model = UrlShortener
        fields = ('long_url','short_url', 'visitor_count')
